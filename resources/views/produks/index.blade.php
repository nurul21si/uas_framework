@extends('default')

@section('content')

	<div class="d-flex justify-content-end mb-3"><a href="{{ route('produks.create') }}" class="btn btn-info">Create</a></div>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>nama</th>
				<th>stock</th>
				<th>harga</th>

				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($produks as $produk)

				<tr>
					<td>{{ $produk->id }}</td>
					<td>{{ $produk->nama }}</td>
					<td>{{ $produk->stock }}</td>
					<td>{{ $produk->harga }}</td>

					<td>
						<div class="d-flex gap-2">
                            <a href="{{ route('produks.show', [$produk->id]) }}" class="btn btn-info">Show</a>
                            <a href="{{ route('produks.edit', [$produk->id]) }}" class="btn btn-primary">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['produks.destroy', $produk->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
					</td>
				</tr>

			@endforeach
		</tbody>
	</table>

@stop
