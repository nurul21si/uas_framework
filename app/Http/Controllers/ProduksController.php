<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\Produk;
use App\Http\Requests\ProdukRequest;

class ProduksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $produks= Produk::all();
        return view('produks.index', ['produks'=>$produks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('produks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProdukRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ProdukRequest $request)
    {
        $produk = new Produk;
		$produk->nama = $request->input('nama');
		$produk->stock = $request->input('stock');
		$produk->harga = $request->input('harga');
        $produk->save();

        return to_route('produks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $produk = Produk::findOrFail($id);
        return view('produks.show',['produk'=>$produk]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $produk = Produk::findOrFail($id);
        return view('produks.edit',['produk'=>$produk]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProdukRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProdukRequest $request, $id)
    {
        $produk = Produk::findOrFail($id);
		$produk->nama = $request->input('nama');
		$produk->stock = $request->input('stock');
		$produk->harga = $request->input('harga');
        $produk->save();

        return to_route('produks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $produk = Produk::findOrFail($id);
        $produk->delete();

        return to_route('produks.index');
    }
}
